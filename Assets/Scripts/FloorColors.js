﻿#pragma strict

function Start () {
	renderer.material.color = Color.red;
}

function Update () {
	renderer.material.color.r = Mathf.Abs(Mathf.Sin((Time.time)/2))/2;
	renderer.material.color.g = Mathf.Abs(Mathf.Sin((Time.time+1)/2))/2;
	renderer.material.color.b = Mathf.Abs(Mathf.Sin((Time.time+2)/2))/2;
}