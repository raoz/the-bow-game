﻿#pragma strict

function OnCollisionEnter (other : Collision) {
	if (other.gameObject.transform.tag=="Hittable"){
		rigidbody.velocity = Vector3.zero;
		rigidbody.useGravity = false;
		rigidbody.isKinematic = true;
		gameObject.GetComponent(Collider).enabled= false;
		if (other.gameObject.name=="Target"){
			other.gameObject.renderer.material.color = Color.red;
			yield WaitForSeconds (1);
			other.gameObject.renderer.material.color = Color.white;
		}
	}

}
var lastPosition : Vector3;
var preLastPosition : Vector3;
function FixedUpdate(){
	
	if(rigidbody.useGravity == false){
		transform.position = preLastPosition;
	} else {
		preLastPosition = lastPosition;
		lastPosition = transform.position;
	}
    
 }