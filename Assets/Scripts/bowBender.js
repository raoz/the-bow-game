﻿#pragma strict

function Start () {
	pullBack = 0;
	animation["ArmatureAction"].enabled=true;
	animation["ArmatureAction"].weight=1;
	animation["ArmatureAction"].speed=0;
}

var pullBack : int;
function Update () {
Debug.Log(animation["ArmatureAction"].speed);
Debug.Log(animation["ArmatureAction"].time);
	//Debug.Log(pullBack);
	var step = animation["ArmatureAction"].length/46;
	if (pullBack!=0) {
		//animation["ArmatureAction"].speed=0;
		animation["ArmatureAction"].time=pullBack*step;
		animation.Sample();
	} else if(animation["ArmatureAction"].time != 0 && animation["ArmatureAction"].speed == 0)
	{
		animation["ArmatureAction"].speed = 1;
		animation["ArmatureAction"].time = 40*step;
	}
	else if(animation["ArmatureAction"].time == 0 && animation["ArmatureAction"].speed == 1)
	{
		Start();
	}
}

/*
function LateUpdate(){
	Debug.Log(pullBack);
	Debug.Log(animation["ArmatureAction"].time);
}*/